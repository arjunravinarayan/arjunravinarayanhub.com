---
layout: page
title: "The Best Herbalife Theory Yet"
---

I've been long confused by the Herbalife case. Bill Ackman took an
enormous short on Herbalife based on a theory that it was a pyramid
scheme. Yet many prominent investors, including Carl Icahn and George
Soros have long positions on Herbalife.

I hadn't seen a comprehensive theory that explains how both viewpoints
can exist... Sure, one of them ultimately is wrong, but none of these
investors are fools. Even if Herbalife is ultimately a sound
corporation, there has to be a clear seductive theory that Herbalife
is a pyramid scheme, and it has to be good enough that Ackman falls
for it. If Herbalife is ultimately a pyramid scheme, there has to be a
clear and compelling theory why that fact is ambiguous to the point
where many sophisticated investors don't see that, even upon closer
examination.

[This
post](http://brontecapital.blogspot.com/2014/03/herbalife-internal-consumption-comment.html)
is the best explanation I have seen yet, and I encourage you to read
the whole thing. The explanation hinges on the fact that internal
consumption in a multilevel marketing company can be good, or bad,
depending on other facts. The immediate response to the idea of
internal consumption is that it's a pyramid scheme. But that's not
necessarily the case, if the internal consumption is good. The
definition of good internal consumption is when satisfied end
consumers sign up as distributors purely to get the distributor
discount, and otherwise remaining consumers. In a cursory analysis,
they will show up as internal consumption, which leads you to suspect
that it is a pyramid scheme.

The [Bronte Capital](http://brontecapital.blogspot.com) blog is
proving itself to be the most interesting new blog on my roll, and I
encourage you to subscribe to it.
