---
layout: post
title: "Assorted Links: Thanksgiving Edition"
category: assorted
excerpt: ""
tags: []
---

1. [How the US Media Would Cover Thanksgiving if It Were in Another Country.](http://www.slate.com/blogs/the_world_/2013/11/27/if_it_happened_there_how_the_u_s_media_would_cover_thanksgiving_if_it_was.html)
2. [Chelsea Manning is thankful for many things.](http://time100.time.com/2013/11/25/time-for-thanks/slide/chelsea-manning/)
3. [Robin Hanson on heroes.](http://www.overcomingbias.com/2013/11/snowden-hero.html) I would alter the plaque to read "We are thankful for..." to fit the theme of the day.
