---
layout: page
title: "About me"
permalink: /about/index.html
group: navigation
---

My name is Arjun Narayan and I am a computer science graduate student
at the University of Pennsylvania. This is my personal blog; you can
find my academic site [here](http://cis.upenn.edu/~narayana/) for a
more conventional presentation of my research and publications.

The blog has an [atom feed](../feed.xml) if you wish to subscribe to my
posts.