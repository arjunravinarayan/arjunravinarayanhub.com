---
layout: post
category: fun
title: "Instrumental Variables"
---


Ok. This is a hard area to see, and I wish there were classes that
actually taught this stuff properly. So Short tour of quantitative
social science: how do you measure if X affects Y?

Basically in economics and sociology direct experimentation is
hard. If you want to prove that x causes y, for example, or that 10%
increase in x cause 20% increase in y, the ideal thing you Want to do
is run a randomized controlled trial. RCT

I.e. form two groups of people, at random. the randomness is important
to make sure that another difference between the groups is unlikely to
happen. For example if group one is in Switzerland and group two is in
france this is bad because maybe frenchness causes a 20% increase in
y. So you find a group, divide them into two at random (so whether
you're French or Swiss doesn't affect which group you get assigned
to), then give one group 10% more x. Now you know the only difference
is that you caused more x, so you can measure y. So you can say that x
causes y for whatever result you find. This is how medical studies are
done. There are things you want to control for, such as blinding,
I.e. the patients don't know which group they are in, if it is a pill,
it is just labeled pill a, and pill b. the doctors Also don't know,
and finally when the experiment is finished, you collect the data and
then Unblind the results, and look at it

This is great in theory and in medicine but sometimes you want to do
experiments on things that you can't control. So if you want to
measure if abortions decrease crime, you can't just put together two
groups and give one group more abortions. or maybe I'll find some
black people and give one group syphillis and see what happens,
I.e. Tuskegee experiments. So what you look for is called "natural
experiments". Say something extraordinary that caused this to
happen. For example, if say two adjacent states legalized abortion at
different times you can compare the towns right on the border.

But you now have to be very careful. Because there might be other
reasons that the towns on the opposite sides of the border are
different. For example, maybe they self select to be on one of the
sides. So a good example is the two sides of Kansas City. A bad
example is Greenwich vs Rye because Greenwich is deliberately on the
other side for specific reasons.

The really bad part is that if there is an underlying variance, then
your study is completely useless. This is called omitted variable
bias, or OVB. And is the bane of all quantitative social studies. The
real problem with OVB is that you never know for sure If you missed
something. Maybe there was some underlying cause that correlates with
one of the groups and you missed it.

Let me give you an example straight out of the freakonomics book. Say
you want to see if charter schools help students. You compare charter
school performance to regular public school. You see charter students
do a lot better. So you conclude that it works. Done? But wait!
There's more!  Now in Chicago, when they started charter schools they
had students enter a lottery. If you won you got to go to the school,
if you lost you went back to the regular school. So you look at the
students who enrolled in the charter schools, this looks like a
perfect experiment. The assignment is random. But some researchers
found that charter school students performed the same as the public
school students WHO LOST THE LOTTERY. I.e. simply entering the lottery
improved your performance. Why? Because it means you have parents who
care. And that improves performance a ton.

So the original experiment was missing a variable: whether you were
entered in the first place by parents who care. The new experiment
found that.

However, maybe there are MORE omitted variables... Who knows? There's
no satisfying answer to this question except textual persuasion. And
now were off the deep end.
