---
layout: post
description: "Apprenticeships are undervalued but surprisingly easy to get"
category: fun
title: "Apprenticeships are Undervalued"
---

Book learning is great, but it takes a lot of work to get to that
understanding.

The only way to learn the "cutting edge" is apprenticeships.

Case method is an attempt to make apprenticeship style learning more
widespread. Mixed results.

Society today does not properly value apprenticeships. Cue Scott Adams
post on bank vice presidency. Grad school is an apprenticeship for
research and thinking. However, there's a huge cliff: nobody wants to
talk/learn from me or other senior grad students. But the moment we
get a tenure track position, students are lining up. This is an
inefficiency.

Fellow grad student with Andreas was telling me how he learned a lot
from working with Andreas. Experience looked very similar to the
training I got 6 months later when Andreas was a professor. Big
discontinuity on "price", not so much on "value".

What other discontinuities can we see?

1) Grad students vastly undervalued. A bit of a lemon market here, as
there are a lot of crap grad students. But easy to see which ones will
become profs (publication records)! Arbitrage!

1a) the fact that this arbitrage doesn't exist shows that academic
knowledge is not that commercially useful (very true).  b) consulting
using grad students does exist, and this is a good step. But still
discontinuity exists.

2) nobody wants to talk to a junior consultant, but they probably are
very valuable sources of mentorship.

3) high school students pick majors to apply/start in college without
any insight. Probably should give them even 1 hour per profession
apprenticeships with various jobs.



Other lessons from my apprenticeship:

1) books are great, but at the cutting edge books haven't been written
yet.

2) learning from research papers is 10x slower than learning from
books. This shows that a well written book has REAL value! imagine
trying to learn calculus from Principia Mathematica. We have learned a
lot not just about calculus, but about the optimal way to teach
calculus. Khan academy shows that there's still low hanging fruit!

3) Apprenticeships are super expensive and don't scale. But they are
as fast as "book learning" if you can get them.


Other thoughts that should go above:

1) one big barrier to apprenticeship is it breaks the illusion of
equality. it requires highly visible subordination. This explains why
a lot of apprenticeships are surrounded in elaborate rituals
(i.e. grad school, employment).

2) thus huge barrier to "trying out apprenticeships". You cant
subordinate unless you are sure the superior has high status. This
explains a lot of the market inefficiency

3) if you can get over this mindset, you can learn a lot. case in
point: I have TAed Ville, and Ville has TAed me in various settings. I
have no issue with subordinating myself to Ville (or he to me). But
this requires a lot of confidence and security in social status.

4) Also seen this extreme status consciousness when I have tutored
friends in math. They are very uncomfortable with the subordination
side of it, even though the gains are very high, very real, and in
retrospect they all agree that it was super valuable. But still won't
do it again.

4a) maybe other reasons as well that have to do with me being a jerk,
but let's not go there (make this a funny joke).

