---
layout: post
description: "How much of what you believe is it acceptable to make publicly known?"
category: fun
title: "Strauss on Reading"
---

"You can't openly say everything you believe because of political
correctness run amok." I imagine that fifty per cent of my social
circle takes that as an obvious fact, and the other fifty recoils at
the implications. After all, if you're only toeing the line on many
flagship political correctness issues (women's rights, racism,
democracy, ...) are you a closet racist/anti-semitic/sexist? It's a
pretty scary proposition. Now, if it's only some small subset of those
views that are wrong, which ones are they? You're not going to get a
straight answer as to which of the conventional wisdom is wrong ---
practitioners of such subterfuge are keeping quiet due to the worries
of blowback. And they're not going to sink their careers, which in
this day and age can be destroyed by one blog post or viral
image. (Aside: I do not subscribe to the opening statement. All
conventional wisdom is definitely correct.)

Nevertheless, it's quite clear that very smart people believe in that
statement. You might not get straight out controversial statements,
but you will get some veiled allusions. Paul Graham has written [a
very readable essay](http://paulgraham.com/say.html) on this
topic. Tyler Cowen makes repeated mentions of [Straussian
readings](http://en.wikipedia.org/wiki/Leo_Strauss#Strauss_on_reading). It's
quite clear that Cowen believes a lot of propositions that he doesn't
put on paper. Cowen's entire oeuvre definitely has a darker undertone
that is cleverly disguised. About the only Silicon Valley/Economist
intellectual who talks openly is Peter Thiel, with his direct
statement that he ["no longer believes that freedom and democracy are
compatible"](http://www.cato-unbound.org/2009/04/13/peter-thiel/education-libertarian).

Given how controversial that essay is, wouldn't you expect some public
distancing from Thiel? Yet Thiel is surprisingly influential in tech
circles: Thiel was Zuckerberg's first major investor, and they are
fairly close. Cowen dedicated his excellent ebook [The Great
Stagnation](http://en.wikipedia.org/wiki/The_Great_Stagnation) to
Thiel, who he credits for "blazing the way".

Thiel's excellent [Stanford course on
startups](http://blakemasters.com/peter-thiels-cs183-startup) included
some gems, such as in [this
class](http://blakemasters.com/post/21437840885/peter-thiels-cs183-startup-class-5-notes-essay)
that explicitly says what I expected to be far more controversial than
it was: don't hire people different from you.

