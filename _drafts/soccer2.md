---
layout: post
category: fun
title: "ESports and Soccer, part 2"
---

Julian writes:

I CANNOT STAND PEOPLE SAYING THE LATEST VIDEO GAME IS BETTER THAN CHESS.

Maybe it is. Usually with books, papers, science, etc. I do the following when I don't have the skills or time to immediately assess the thing myself: wait 10 years after it's original release/publication, see if people I respect still think it's worth reading/studying, and then study it. Chess passes, just about every video game doesn't. 

Sundry points:

I'm  suspicious of your suspense argument. In chess and go, there is a style of playing that is associated with a mental process that goes roughly as follows:

  "Ohh, if I play this, and my opponent doesn't see the threat, I'll capture her Queen!"

And there's suspense ("will my opponent see it?"), which some people find entertaining. And it's the lowest form of chess.

[It does make it easy to watch, though, if you're not willing to put in the time and effort for higher pleasures. I know I'm not - I often watch bits of street chess but very little of world championships. I know why, but I don't think I have admirable reasons for it.]

I'm going to bet there's at least one major opening innovation in Anand-Carlsen.

Many rhymes have been used before in English. Let's make up a new language.

Go has changed less than chess, and is older. It really hasn't been explored. Are we playing Starcraft because we're so intelligent that we thing it has, or because the entire fucking generation is ADD and can't play a game where it might happen that nothing on the board/screen changes for as long as five minutes? 

Next up, why Beck is better than Beethoven?

Reply.