---
layout: page
title: "Review of 'The Hard Thing about Hard Things' by Ben Horowitz"
excerpt: ""
---

Ben Horowitz wrote a book. It's okay.

Executive Summary: This is a book about Ben Horowitz's war
stories. Ben Horowitz has good war stories, if you care about the
narrow space of Venture Backed fast growth technology startups. I'm
not so sure that they generalize to the point of making a good
management guide. You might be better off reading Drucker.

First: the absolute preliminaries: Ben Horowitz co-founded LoudCloud
with Marc Andreessen in 1999, with a plan to do enterprise managed
services (what has now grown to the SaaS and IaaS space). They were
technological pioneers in that space. They weathered the dot com
bubble burst, but it was touch and go (since most of their clients
were other tech firms going bankrupt). They IPOd in a post-bubble
environment with a declining market. They pivoted to become Opsware, a
tech services firm (i.e. further up in the backend of the managed
services stack). They pivoted as a public corporation close to
bankruptcy which is a nightmare I wouldn't wish on my worst
enemies. They weathered NASDAQ delisting threats, mass employee
revolts, ..., the whole Mad Max scenario. So he offers the prospect of
excellent data from the inside. Ben also has a reputation in the
valley as a no-bullshit guy, so this is valuable

Second: some initial thoughts. To the extent that Ben's stories will
generalize, they only apply to fast growth venture backed tech
startups that plan to IPO. It bears repeating that this is but one
subset of the technology space - the subset that gets a
disproportionately large amount of press coverage and fanfare. So much
of the lessons are the kind that you've probably already osmosed from
the Paul Graham/Peter Thiel/A360Z/AVC crowd. And if you haven't, then
you should probably start with Paul Graham and Peter Thiel rather than
jumping into this book, which has a narrower scope and doesn't make
much effort to make clear the context. That's okay, since it's aimed
at the specific crowd, but just be aware if you're not of that crowd
and thinking of reading this book.

Ben tells the story from the technical CEO perspective, which is a
fairly rare perspective to get, given the depth of Ben's experience:
Note that Paul Graham only walked the walk through a ~50 million
dollar acquisition, and never faced the challenges of being the CEO of
a publicly traded corporation. Peter Thiel did, but is ultimately a
non-technical founder. Besides, the PayPal mafia is fairly cagey about
the war stories around PayPal given the extent of the internal fights
(e.g. the ouster of Elon Musk from CEO) that they want to cover
up. Much of the other available writings are from the
V.C. perspective, and lack founder experience.

Thus, this book does give us an opportunity to read the thoughts of a
founder CEO all the way through and beyond the IPO stage. But it is by
no means a masterpiece that stands alone. It does offer an incremental
view if you are primarily interested in the narrow niche of venture
backed startups, and serves as a 4 star or 5 star book of anecdotes
that have potentially generalizable lessons, if read critically, while
fully aware of the appropriate broader context. That's a big asterisk
though. So I will attempt to summarize the core lessons that I
personally got out of the books, given that I am not solely interested
in this space:

1. Being CEO in a fast growth venture startup requires learning at an
incredible pace. The job description changes almost completely within
6 months given the growth rate. There isn't any way to get that
experience except networking with founders who have previous lived
experience as founder CEOs of large companies. This is why, for
example, Zuckerberg had long walks with Jobs. A top notch VC firm will
definitely help you get that, which is why they are so valuable. The
rotating board circuit in the top Silicon Valley firms will provide
this, if you can get them to sit on your board (Horowitz, for example,
holds Bill Campbell's advice in particularly high esteem). You
probably can't, unless Sequioa, Founders Fund, Andreessen-Horowitz, or
KPCB is backing you.

2. At the end of the day, you have to make incredibly complex
strategic decisions with extremely limited information, which creates
crazy stressful environments.

3. To Ben's credit, he does focus on good management, but mostly
doesn't add anything new to the corpus. It's mostly the usual good
stuff: don't do arbitrary things, don't publicly shame people, don't
let people get away with enriching themselves at the company's expense
through strong arming, don't do short term things that jeopardize the
long term, and if you do, it will rapidly become the culture and will
remain permanently broken. Again, a decent coverage of the standard
MBA literature covers this space better.

4. Being CEO is a lonely job. You cannot expect your executive team to
understand all your decisions given their limited scope. You also
cannot spend the time to give them the broader scope all the time,
since that takes away from doing their own jobs. But if you don't,
they might stop trusting you. It's Catch-22s all the way down.

5. In addition, while the CEO mentor network can help with your
emotions, they can't help you with the decisions, since it will take
them weeks to get up to speed on the context. You're really on your
own on these decisions.

6. It's also emotionally harder when your final decision is 55/45, and
you're facing opposition from underlings/your board that is 90/10 the
other way, but lack the appropriate context. So you're on the fence
leaning one way, but others are convinced that you're wrong. At this
point, you should just learn to trust yourself, since you're the only
one with the appropriate context. Your board should be more
experienced than you, so they should be aware of this information
asymmetry. Consequentially, they'll usually just rubberstamp what you
decide anyway. This doesn't make the decisions any easier to make.

7. You absolutely need to develop decision making processes that are
insulated from your own emotional rollercoaster/insecurity. Ben likes
writing things down until the words are convincing on their own. I do
too. Whatever your methods are, you will absolutely need to rely on
them since you're going to be alone in the decision making processes,
and need some check to make sure you're operating logically with no
emotional biases. So develop those early. (I personally recommend the
LessWrong corpus, but you might find that too kooky. Your choice.)

8. Ben Horowitz really likes rap and hiphop. That's not my thing, but
whatever, I can respect his artistic side. Anyone who criticizes him
for that aspect of his personality is really missing the point.

Context that Ben Horowitz did not mention that I think would have
greatly improved the book:

1. The tides are turning, and the days when Venture Capitalists could
oust founder CEOs and replace them with "adults" are permanently
gone. Zuckerberg, and the fact that founders (like Horowitz and Thiel)
now lead top Valley VC firms has permanently changed the landscape. So
a lot of the fighting that Ben had to do, and the perpetual
underconfidence that he did it with, won't apply to future founder
CEOs. This book adds to the growing corpus that helps make your case
to not be ousted, which partially obsoletes that aspect of lessons in
the book. Ironic, I know.

2. Fast growth tech startups that have a distinctive tech advantage
can win despite terribly dysfunctional management. When you're in a
greenfield technology space, you can do whatever you want, and still
win just from the tech advantage. For example, Valve gets cited a lot
for it's "no management" structure, but they sit on a giant money
spigot called Steam that just prints money. If I also owned such an
oil derrick, I could probably get away with mandatory sex parties
every Thursday. This doesn't make the case for workplace orgies.

3. There is great talk about how Ben rescued OpsWare from near
disaster through their final acquisition by HP for 1.65 billion
dollars. But there is not a single mention of how OpsWare fared
post-acquisition. For all I know (and given the next point, I suspect)
this was a shitty deal for HP.

4. It doesn't help that Ben Horowitz heavily raided the OpsWare
executive team to form the core team at his next venture (the VC firm
Andreessen-Horowitz). That can't have been good for OpsWare at all.

5. Ben talks about how you really need your executive team to hit the
ground running. You can cover for one executive to get them up to
speed, but you can't cover for every executive. Thus, you need to hire
top tier talent, often from outside. This is great. But this also
forms a good explanation for why top executives often seem to be from
a different planet, move in completely different circles, and get paid
orders of magnitude more. But Ben doesn't cover any of these
pathologies, or how it really is a necessary side-effect of the
realities of the executive responsibilities.

6. From Ben's raiding of his previous form for top executive talent, I
get an increasing suspicion that the top talent moves in tight-knit
teams that gel well together. Your management priority should be to
network and form these connections from an early stage (definitely
before you found your own VC-backed fast growth startup). If you don't
have this focus, you have to develop it at a late stage in the game,
and for that you _really_ need top VCs, otherwise you will not be
plugged in at all and have no access to these folk.