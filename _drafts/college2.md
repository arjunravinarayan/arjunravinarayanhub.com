---
layout: post
category: fun
title: "College is Consumption part 2"
---

Trevor writes: But most of this college over-consumption, I think, is
funded by alumni not students. You pay 40k a year for class, rent,
food, etc. On top of that you have access to fancy buildings,
subsidized museums, and nice gyms. I don't think these luxuries
represent obvious misappropriation of investment -- i.e. the students
aren't paying for it -- because this consumption is actually funded by
alumni donations that are earmarked for specific consumption
purposes. Perhaps we could limit earmarking, so that administrators
could direct all funds away from luxury consumption, but that would
reduce the pool of donations (potentially desirable). If I donate 50m,
and they build a student center with my name on it, it better be
goddamn fancy.

Frank Morgan writes: 

1.        http://math.williams.edu/decisions-and-priorities/

2. The only reply I can think of is that most of the luxuries are
funded by alum gifts or pay for themselves in alum gift revenues.

Response required...
