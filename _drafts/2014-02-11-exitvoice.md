---
layout: page
title: "Exit, Voice, and Marc Andreessen"
customjs:
 - http://platform.twitter.com/widgets.js
---

A pmarca Twitter Rant
---

Yesterday, Marc Andreessen ([pmarca](https://twitter.com/pmarca) on
Twitter), famed entrepreneur and Silicon Valley venture capitalist,
went on one of his infrequent Twitter rants (where he just posts
stream of consciousness information dumps). This particular Twitter
rant was concerned with shareholder value and

<blockquote class="twitter-tweet" data-conversation="none" data-cards="hidden" lang="en"><p>Marc&#39;s theory of
public company shareholder bases--overgeneralized to be
useful--[disclaimer: nothing to do w/any co where I&#39;m on
board]:</p>&mdash; Marc Andreessen (@pmarca) <a
href="https://twitter.com/pmarca/statuses/432590467989905408">February
9, 2014</a></blockquote> 

<blockquote class="twitter-tweet" lang="en"><p>At any given time, public co&#39;s shareholder base consists entirely of one of: growth investors, value investors, arbitrageurs, or nobody.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432590873528782848">February 9, 2014</a></blockquote>

<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>Growth investors only value growth; will hold stock while revenue is up &amp; to the right, will dump stock immediately when growth slows.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432591149404930050">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>Value investors only value free cash flow; will hold stock while FCF is growing or stable, will dump stock when free cash flow falls.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432591664310280193">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>Arbitrageurs only value near-term opportunity for company to sell out or otherwise recapitalize, will dump stock when nothing happens.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432591911971336192">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>Therefore natural shareholder base for a co is &quot;nobody&quot;: if not growth, value, or arbitrageurs, then stock flatlines at ~1/2 cash in bank.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432592146126745600">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>Many newly public co&#39;s follow pattern: go IPO w/growth investors, revenue slows so rotate to value investors; then arbitrageurs force sale.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432592421449248769">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>For maturing company, very easy to go from growth to value investors; very hard to go from value to growth investors.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432592701326778368">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>Specific challenge in tech: value investors don&#39;t want speculative investment in new tech, but without, hard to maintain profits over time.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432593064402493441">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>So tech co&#39;s with value investors can fall into a form of mutual death grip: don&#39;t invest--&gt;tech changes--&gt;profits fall--&gt;don&#39;t invest.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432593568025165825">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>So tech co that wants to innovate over time needs growth investors, but needs to justify w/continuous revenue growth; execution critical.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432593793481588737">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>Meanwhile, since tech P/E multiples at generational lows, tech is viable hunting ground for arbitrageurs today more than last 30 years.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432594309913665536">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>Something dramatic about to happen in tech: tons of M&amp;A, or huge upward move in P/E multiples, or revenue collapse of many public tech co&#39;s.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432594792501895168">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>For public co/modern era, everything connected: stock price, customer receptivity, morale &amp; talent flows, press coverage, ecosystem trust.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432622870099804160">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>As result:Public co tends to either positive feedback cycle where everything works well, or negative feedback cycle where everything is bad.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432623191467360256">February 9, 2014</a></blockquote>


<blockquote class="twitter-tweet" lang="en" data-conversation="none" data-cards="hidden"><p>Therefore momentum -- in overall sense -- has become key: leads to either compounding goodness or compounding badness.</p>&mdash; Marc Andreessen (@pmarca) <a href="https://twitter.com/pmarca/statuses/432623389224607744">February 9, 2014</a></blockquote>




Exit, Voice, and Loyalty
----

[Exit, Voice, and
Loyalty](http://en.wikipedia.org/wiki/Exit,_Voice,_and_Loyalty) is a
1970 book by the social scientist Albert O. Hirschman. It's an easy
read, and at only 100 pages, is a must-read for any budding social
scientist, especially those that are bad at math and are fast losing
any comparative advantage in the discipline (you know who you
are).


The basic thesis of Exit, Voice, and Loyalty is that dissatisfied
members of any organization have two choices to remedy their
situation: they can _exit_, by leaving the organization, or use their
_voice_ to effect change. Which choice they take ultimately depends on
a host of external factors, but the fundamental duality is that exit
is economic (the free market disciplines actors due to easy exits and
the constant threat of exit), and voice is political. Voice is in some
sense more desirable, since it is high bandwidth. An exit only gives
you one bit of information. Exit, in many real world situations, tends
to be more available to the wealthy, which can often result in
suboptimal separation (e.g. white flight). But suppressing exit
doesn't solve that problem as it is often the threat of exit that
makes the voice credible in effecting social change.

Some practical examples: 

As an asocial type, I obviously have strong preferences for the
former.