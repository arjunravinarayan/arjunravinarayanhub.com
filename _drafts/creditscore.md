---
layout: page
title: "A Distributed Database for Credit Scoring"
---

The [FICO
score](https://en.wikipedia.org/wiki/Credit_score_in_the_United_States#FICO_score)
is a widely used metric for determining the creditworthiness of
borrowers. Everyone who has a credit card, a mortgage, or any sort of
loan, has a FICO score. Essentially, the FICO score communicates 